# Setup


## Build and run guest-connector-io 

#### Clone the project
`git clone git@bitbucket.org:Traction/guest-connector-io.git && cd guest-connector-io`

`docker-compose up`

#### More details at:
https://bitbucket.org/Traction/guest-connector-io


## Build and run test_traction

Tests are developed using [Gauge](http://getgauge.io/index.html).

#### Clone the project
`git clone git@bitbucket.org:maristn/test_traction.git && cd test_traction`

#### Build the project
`docker-compose build`

#### Execute all the tests
`docker-compose run --rm gauge run --verbose specs`


## Examples

#### Execute a set of specs
`docker-compose run --rm gauge run --verbose specs/get_validations.md`


#### Execute tests by flag
`docker-compose run --rm gauge run --verbose --tags regression specs`


#### Rerun failed tests
Just add `--failed` before the specs, ex.:
`docker-compose run --rm gauge run --failed`
